from operator import itemgetter

from flask import render_template, request
import requests
import datetime
from flaskapp.forms import WeatherDataForm
from flaskapp.models import WeatherData
from flaskapp import app, db


@app.route("/", methods=['GET', 'POST'])
def home():
    form = WeatherDataForm()
    if request.method == "POST" and form.validate():
        delta_time = (
                abs(form.beginning_date.data - datetime.datetime.now().date()).days <= 7 and
                abs(form.end_date.data - datetime.datetime.now().date()).days <= 7
        )
        if delta_time and (form.end_date.data - form.beginning_date.data).days > 0:
            qs = WeatherData.query.filter_by(city_code=form.city_code.data,
                                             beginning_date=form.beginning_date.data,
                                             end_date=form.end_date.data).first()
            if qs:
                context = {
                    'city_code': form.city_code.data,
                    'beginning_date': form.beginning_date.data,
                    'end_date': form.end_date.data,
                    'max_temperature': qs.max_temperature,
                    'min_temperature': qs.min_temperature,
                    'rain_probability': qs.rain_probability,
                    'amount_of_rain': qs.amount_of_rain
                }
                return render_template('temperature.html', context=context)
            else:
                my_token = '81223131ae7a3f3a1f74cf13f15598c0'
                response = requests.get(
                    f'http://apiadvisor.climatempo.com.br/api/v1/forecast/locale/{form.city_code.data}/days/15?token={my_token}'
                )
                json_object = response.json()
                # Temperature Averages
                max_temp = max([x['temperature']['max'] for x in json_object['data']])
                min_temp = min([x['temperature']['min'] for x in json_object['data']])
                max_temperature_dict = list(filter(lambda x: x['temperature']['max'] == max_temp, json_object['data']))
                min_temperature_dict = list(filter(lambda x: x['temperature']['min'] == min_temp, json_object['data']))
                max_temp_dates = [i['date'] for i in max_temperature_dict]
                min_temp_dates = [i['date'] for i in min_temperature_dict]
                # Rain Averages
                max_prob = max([x['rain']['probability'] for x in json_object['data']])
                max_probability_dict = list(filter(lambda x: x['rain']['probability'] == max_prob, json_object['data']))
                max_prob_dates = [i['date'] for i in max_probability_dict]
                max_prob_precipitation = [i['rain']['precipitation'] for i in max_probability_dict][0]
                weather = WeatherData(city_code=form.city_code.data,
                                      beginning_date=form.beginning_date.data,
                                      end_date=form.end_date.data,
                                      max_temperature=max_temp,
                                      min_temperature=min_temp,
                                      rain_probability=max_prob,
                                      amount_of_rain=max_prob_precipitation)
                db.session.add(weather)
                db.session.commit()
                context = {
                    'city_code': form.city_code.data,
                    'beginning_date': form.beginning_date.data,
                    'end_date': form.end_date.data,
                    'max_temperature_dates': max_temp_dates,
                    'max_temperature': max_temp,
                    'min_temperature_dates': min_temp_dates,
                    'min_temperature': min_temp,
                    'rain_probability_dates': max_prob_dates,
                    'rain_probability': max_prob,
                    'amount_of_rain': max_prob_precipitation
                }
                return render_template('temperature.html', context=context)
        else:
            context = {
                'error': 'Dados incorretos!'
            }
            return render_template('temperature.html', context=context)
    return render_template('index.html', form=form)
