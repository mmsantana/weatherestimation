from flask_wtf import FlaskForm
from wtforms import IntegerField, DateField, SubmitField
from wtforms.validators import DataRequired


class WeatherDataForm(FlaskForm):
    city_code = IntegerField('Código da Cidade',
                             validators=[DataRequired()])
    beginning_date = DateField('Data de Início',
                               validators=[DataRequired()])
    end_date = DateField('Data de Fim')
    submit = SubmitField('Pesquisar')
