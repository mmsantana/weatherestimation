from flaskapp import db
from sqlalchemy import PrimaryKeyConstraint


class WeatherData(db.Model):
    __table_args__ = (
        PrimaryKeyConstraint('city_code', 'beginning_date', 'end_date'),
    )

    city_code = db.Column(db.Integer)
    beginning_date = db.Column(db.Date)
    end_date = db.Column(db.Date)
    max_temperature = db.Column(db.Float)
    min_temperature = db.Column(db.Float)
    rain_probability = db.Column(db.Float)
    amount_of_rain = db.Column(db.Float)

    def __repr__(self):
        return f'Id da cidade: {self.city_code} / First Date: {self.beginning_date} / End Date: {self.end_date}'
